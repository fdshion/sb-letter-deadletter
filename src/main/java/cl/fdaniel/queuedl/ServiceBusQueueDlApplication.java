package cl.fdaniel.queuedl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServiceBusQueueDlApplication {

    public static void main(String[] args) {
        SpringApplication.run(ServiceBusQueueDlApplication.class, args);
    }

}
