package cl.fdaniel.queuedl.config;

import com.azure.messaging.servicebus.ServiceBusProcessorClient;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class SBListenerLetterConfig implements InitializingBean{

    private final ServiceBusProcessorClient serviceBusProcessorClient;

    public SBListenerLetterConfig(@Qualifier("letter")
                                ServiceBusProcessorClient serviceBusProcessorClient) {
        this.serviceBusProcessorClient = serviceBusProcessorClient;
    }

    @Override
    public void afterPropertiesSet() {
        serviceBusProcessorClient.start();
    }
}
