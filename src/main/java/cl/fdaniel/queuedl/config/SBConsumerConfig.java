package cl.fdaniel.queuedl.config;

import cl.fdaniel.queuedl.services.QueueService;
import com.azure.messaging.servicebus.ServiceBusClientBuilder;
import com.azure.messaging.servicebus.ServiceBusErrorContext;
import com.azure.messaging.servicebus.ServiceBusProcessorClient;
import com.azure.messaging.servicebus.models.ServiceBusReceiveMode;
import com.azure.messaging.servicebus.models.SubQueue;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
public class SBConsumerConfig {

    @Value("${azure.sb.connectionString}")
    private String connectionString;
    @Value("${azure.sb.queueName}")
    private String queueName;

    private final QueueService queueService;


    @Bean
    @Qualifier("letter")
    public ServiceBusProcessorClient letterProcessorClient() {
        return new ServiceBusClientBuilder()
                .connectionString(connectionString)
                .processor()
                .queueName(queueName)
                .receiveMode(ServiceBusReceiveMode.PEEK_LOCK)
                .disableAutoComplete()
                .processMessage(queueService::processMessage)
                .processError(SBConsumerConfig::processError)
                .disableAutoComplete()
                .buildProcessorClient();
    }
    @Bean
    @Qualifier("deadLetter")
    public ServiceBusProcessorClient deadLetterProcessorClient() {
        return new ServiceBusClientBuilder()
                .connectionString(connectionString)
                .processor()
                .queueName(queueName)
                .subQueue(SubQueue.DEAD_LETTER_QUEUE) // para consumir de la cola deadletter
                .receiveMode(ServiceBusReceiveMode.PEEK_LOCK)
                .disableAutoComplete() //
                .processMessage(queueService::processDeadLetterMessage)
                .processError(SBConsumerConfig::processError)
                .disableAutoComplete()
                .buildProcessorClient();

    }

    private static void processError(ServiceBusErrorContext context) {
        System.out.println("SBConfig.processError");
    }

}
