package cl.fdaniel.queuedl.config;

import com.azure.messaging.servicebus.ServiceBusClientBuilder;
import com.azure.messaging.servicebus.ServiceBusSenderClient;
import com.azure.messaging.servicebus.administration.ServiceBusAdministrationClient;
import com.azure.messaging.servicebus.administration.ServiceBusAdministrationClientBuilder;
import com.azure.messaging.servicebus.administration.models.QueueProperties;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.Duration;

@Configuration
public class SBProducerConfig {

    @Value("${azure.sb.connection-string}")
    private String connectionString;
    @Value("${azure.sb.queue-name}")
    private String queueName;

    @Bean
    public ServiceBusSenderClient subscribeServiceBusSenderClient() {
        return new ServiceBusClientBuilder()
                .connectionString(connectionString)
                .sender()
                .queueName(queueName)
                .buildClient();
    }

//    @Bean
//    void adminiAdministrationClientUpdateQueue() {
//        ServiceBusAdministrationClient client = new ServiceBusAdministrationClientBuilder()
//                .connectionString(connectionString)
//                .buildClient();
//        QueueProperties properties = client.getQueue(queueName);
//
//        System.out.printf("Before queue properties LockDuration: [%d seconds], Max Delivery count: [%d].%n",
//                properties.getLockDuration().getSeconds(), properties.getMaxDeliveryCount());
//
//        // You can update 'QueueProperties' object with properties you want to change.
//        properties.setMaxDeliveryCount(3).setLockDuration(Duration.ofSeconds(10));
//
//        QueueProperties updatedProperties = client.updateQueue(properties);
//
//        System.out.printf("After queue properties LockDuration: [%d seconds], Max Delivery count: [%d].%n",
//                updatedProperties.getLockDuration().getSeconds(), updatedProperties.getMaxDeliveryCount());
//
//    }


}
