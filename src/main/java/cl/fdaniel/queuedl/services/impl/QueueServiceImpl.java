package cl.fdaniel.queuedl.services.impl;

import cl.fdaniel.queuedl.dto.MessageRequest;
import cl.fdaniel.queuedl.services.QueueService;
import com.azure.messaging.servicebus.*;
import com.azure.messaging.servicebus.models.DeadLetterOptions;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;


@Service
@RequiredArgsConstructor
public class QueueServiceImpl implements QueueService {

    private final ServiceBusSenderClient serviceBusSenderClient;

    @Override
    public void sendToQueue(MessageRequest request) {

        ServiceBusMessage message = new ServiceBusMessage(request.message());
        serviceBusSenderClient.sendMessage(message);
    }

    @Override
    public void processMessage(ServiceBusReceivedMessageContext context) {
        final ServiceBusReceivedMessage message = context.getMessage();
        long deliveryCount = message.getDeliveryCount();
        CountDownLatch countdownLatch = new CountDownLatch(1);

        final boolean success = Math.random() < 0.1;
        if (success) {
            try {
                countdownLatch.await(5, TimeUnit.SECONDS);
                context.complete();
                System.out.println(message.getBody() + " Complete");
                System.out.println("deliveryCount = " + deliveryCount);
            } catch (Exception completionError) {
                System.out.printf("Completion of the message %s failed\n", message.getMessageId());
                completionError.printStackTrace();
            }
        } else {
            try {
                DeadLetterOptions letterOptions = new DeadLetterOptions();
                letterOptions.setDeadLetterReason("tu como estas");
                letterOptions.setDeadLetterErrorDescription("Estoy bien");
                context.deadLetter(letterOptions);
                context.abandon();
                System.out.println(message.getBody() + " abandon");
                System.out.println("deliveryCount = " + deliveryCount);

            } catch (Exception abandonError) {
                System.out.printf("Abandoning of the message %s failed\n", message.getMessageId());
            }
        }


    }

    @Override
    public void processDeadLetterMessage(ServiceBusReceivedMessageContext serviceBusReceivedMessageContext) {
        System.out.println("serviceBusReceivedMessageContext.getMessage().getBody() = " + serviceBusReceivedMessageContext.getMessage().getBody());
        System.out.println("serviceBusReceivedMessageContext.getMessage().getDeadLetterReason() = " + serviceBusReceivedMessageContext.getMessage().getDeadLetterReason());
        System.out.println("serviceBusReceivedMessageContext.getMessage().getDeadLetterErrorDescription() = " + serviceBusReceivedMessageContext.getMessage().getDeadLetterErrorDescription());
        serviceBusReceivedMessageContext.complete();
        System.out.println(serviceBusReceivedMessageContext.getMessage() + " Complete");
    }
}
