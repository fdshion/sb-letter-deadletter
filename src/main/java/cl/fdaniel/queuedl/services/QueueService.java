package cl.fdaniel.queuedl.services;

import cl.fdaniel.queuedl.dto.MessageRequest;
import com.azure.messaging.servicebus.ServiceBusReceivedMessageContext;

public interface QueueService {

    void sendToQueue(MessageRequest request);

    void processMessage(ServiceBusReceivedMessageContext context);

    void processDeadLetterMessage(ServiceBusReceivedMessageContext serviceBusReceivedMessageContext);
}
