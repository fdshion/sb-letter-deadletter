package cl.fdaniel.queuedl.controller;

import cl.fdaniel.queuedl.dto.MessageRequest;
import cl.fdaniel.queuedl.services.QueueService;
import com.azure.messaging.servicebus.ServiceBusProcessorClient;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import java.util.concurrent.TimeUnit;

import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping(QueueController.CONTEXT_PATH)
public class QueueController {
    public static final String CONTEXT_PATH = "/v1";
    public static final String SEND_MESSAGE = "/send-message";
    public static final String DEAD_LETTER = "/dead-letter";

    private final QueueService queueService;
    private final ServiceBusProcessorClient serviceBusProcessorClient;

    public QueueController(QueueService queueService,
                           @Qualifier("deadLetter")
                           ServiceBusProcessorClient serviceBusProcessorClient) {
        this.queueService = queueService;
        this.serviceBusProcessorClient = serviceBusProcessorClient;
    }


    @ResponseStatus(NO_CONTENT)
    @PostMapping(SEND_MESSAGE)
    public void sendMessage(@RequestBody MessageRequest request) {
        queueService.sendToQueue(request);
    }

    @ResponseStatus(OK)
    @PostMapping(DEAD_LETTER)
    public void processDeadLetterQueue() throws InterruptedException {

        System.out.println("Starting deadLetter processor");
        serviceBusProcessorClient.start();

        TimeUnit.SECONDS.sleep(10);
        System.out.println("Stopping and closing deadLetter processor");
        serviceBusProcessorClient.close();

    }
}
